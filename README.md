# Messages generator
Welcome to this project, forked from `https://framagit.org/pleinlavue/generateur-d-email`. The goal is to have a website creating donation messages for our activists.

## Functionalities
- Create texts for emails.
- Copy the formatted text
- Send it with the mail application of your device (text not formatted)

## Technologies
Very simple technologies are used here: HTML, CSS and Javascript (vanilla).

## Known Issues

## Improvments
- Add best practices on codes
- Add commit hooks
- Add social network sharing possibilities
- Choose your local group
- Add tracking
- Directly modify the text of emails
