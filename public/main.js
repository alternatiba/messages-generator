var subject = 'Pourquoi j\'aimerais bien que tu fasses un don à Alternatiba'

function onLoad() {
    document.getElementById('arguments').addEventListener('click', updateEmailToCopy)
    document.getElementById('arguments').addEventListener('click', updateEmail)
    document.getElementById('name').addEventListener('change', updateEmailToCopy)
    document.getElementById('name').addEventListener('change', updateEmail)
    document.getElementById('send-email').addEventListener('click', showSendTip)
    document.getElementById('showEmail').addEventListener('click', showEmail)
    document.getElementById('word-gender').addEventListener('click', updateGenderTexts)
    document.getElementById('added-text').addEventListener('input', updateEmailToCopy)
    document.getElementById('added-text').addEventListener('input', updateEmail)
    document.getElementById('word-gender').addEventListener('click', updateEmailToCopy)
    document.getElementById('word-gender').addEventListener('click', updateEmail)
    updateGenderTexts()
    updateEmail()
    updateEmailToCopy()
}

function updateGenderTexts() {
    var gender = document.querySelector('input[name="wordGender"]:checked').value
    var texts = JSON.parse(gender == "f" ? textsF : textsM)
    for (var key in texts) {
        document.getElementById(key).innerHTML = texts[key]
    }
}

function buildMailtoHref(body) {
    return 'mailto:' + '?subject=' + encodeURIComponent(subject) + '&body=' + encodeURIComponent(body)
}

function setMailtoHref(body) {
    var href = buildMailtoHref(body)
    document.getElementById('send-email').attributes.href.value = href
}

function CopyToClipboard(containerid) {
    if (document.selection) {
        var range = document.body.createTextRange();
        range.moveToElementText(document.getElementById(containerid));
        range.select().createTextRange();
        document.execCommand("copy");
    } else if (window.getSelection) {
        var range = document.createRange();
        range.selectNode(document.getElementById(containerid));
        window.getSelection().addRange(range);
        document.execCommand("copy");
        alert("Email Copié, tu n'as plus qu'à le coller dans ta boite mail, le modifier à ta façon et l'envoyer aux personnes de ton choix !")
    }
}

function updateEmailToCopy() {
    var body = replaceTripleDots(buildBody(isHtml = true))
    document.getElementById('email-to-copy').innerHTML = body
}

function updateEmail() {
    var body = replaceTripleDots(buildBody(isHtml = false))
    setMailtoHref(body)
    document.getElementById('subjectText').innerHTML = subject
    document.getElementById('bodyText').innerHTML = body.replace(/\n/g, '\n\n')
}

// Getters

function getSelectedArgumentsText(isHtml) {
    var elements = document.querySelectorAll('#arguments li')
    if (isHtml) {
        var argumentsList = [].filter.call(elements, function(element) {
                return element.childNodes[0].checked
            })
            .map(function(element) {
                return "<li style='display: list-item; list-style-type: disc'>" + element.textContent + "</li>"
            })
            .join("")
        return "<ul style='margin-left: 40px;'>" + argumentsList + "</ul>"
    } else {
        return [].filter.call(elements, function(element) {
                return element.childNodes[0].checked
            })
            .map(function(element) {
                return "\t • " + element.textContent
            })
            .join("\n\n")
    }

}

function getField(id) {
    var element = document.getElementById(id)
    return element.value || element.attributes.placeholder.value
}

function getTextFromElement(id, isHtml) {
    var element = document.getElementById(id)
    return isHtml ? element.innerHTML : element.textContent
}

// Others
function buildBody(isHtml = false) {
    var lineBreak = isHtml ? '<br><br>' : '\n\n'

    var addedText = document.getElementById('added-text').value
    if (addedText) {
        addedText = lineBreak + addedText
    }

    return getTextFromElement('intro-hello', isHtml) + lineBreak + getTextFromElement('intro-text', isHtml) + lineBreak + getSelectedArgumentsText(isHtml) + lineBreak + getTextFromElement('conclusion-text', isHtml) + addedText + lineBreak + getTextFromElement('conclusion-greetings', isHtml) + lineBreak + getField('name')
}

function replaceTripleDots(text) {
    return text
        // Replace the first instance
        .replace('…', ' : ')
        // and all the others
        .replace(/… /g, '- ')
}

function hide(id) {
    var element = document.getElementById(id)
    element.className += ' hidden'
}

function show(id, className) {
    var element = document.getElementById(id)
    element.className = element.className.replace('hidden', '').trim()
}

function showSendTip() {
    show('preparingEmailFailedTip')
    hide('emailCanBeModifiedTip')
}

function showEmail(event) {
    document.getElementById('email').className = ''
    event.preventDefault()
}

document.addEventListener('DOMContentLoaded', onLoad, false)

console.log('Salut ! Pour tout rapport de bug ou suggestion : https://framagit.org/alternatiba/messages-generator/-/issues')
